﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SigmaStudioProjectWS.Startup))]
namespace SigmaStudioProjectWS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
