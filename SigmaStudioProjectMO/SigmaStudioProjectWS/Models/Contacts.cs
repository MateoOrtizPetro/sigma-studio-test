﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiMessages
{
    public class Contacts
    {
        public string name { get; set; }
        public string email { get; set; }
        public string state { get; set; }
        public string city { get; set; }
    }
}
