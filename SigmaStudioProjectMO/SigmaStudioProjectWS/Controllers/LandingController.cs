﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ApiMessages;
using System.Net.Http;

namespace SigmaStudioProjectWS.Controllers
{
    public class LandingController : Controller
    {
        // GET: Landing
        public ActionResult Index()
        {
            var states = getStatesFromApi();
            var ListDWStates = states
               .Select(x =>
                       new SelectListItem
                       {
                           Value = x.state,
                           Text = x.state
                       });

            ViewBag.ListState = ListDWStates;

            if (TempData["statusResponse"]!=null) {
                ViewBag.status = TempData["statusResponse"].ToString();
            }
            
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadCitiesByDepartment(string state)
        {
            var listCities = getCitiesFromApi(state).city;
            var selectListItems = listCities.Select(x => new SelectListItem() { Value = x, Text = x }).ToList();
            return Json(selectListItems, JsonRequestBehavior.AllowGet);
        }

        private List<Departments> getStatesFromApi()
        {
            try
            {
                var resultList = new List<Departments>();
                var client = new HttpClient();
                var getDataTask = client.GetAsync("http://localhost:54952/api/Departments/").
                    ContinueWith(
                            response =>
                            {
                                var result = response.Result;
                                if (result.StatusCode == System.Net.HttpStatusCode.OK) {
                                    var readResult = result.Content.ReadAsAsync<List<Departments>>();
                                    readResult.Wait();
                                    resultList = readResult.Result;
                                }
                            }
                    );
                getDataTask.Wait();
                return resultList;
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        private Departments getCitiesFromApi(string state)
        {
            try
            {
                var resultList = new Departments();
                var client = new HttpClient();
                var getDataTask = client.GetAsync(string.Format("http://localhost:54952/api/Departments?state={0}", state)).
                    ContinueWith(
                            response =>
                            {
                                var result = response.Result;
                                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    var readResult = result.Content.ReadAsAsync<Departments>();
                                    readResult.Wait();
                                    resultList = readResult.Result;
                                }
                            }
                    );
                getDataTask.Wait();
                return resultList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool createContact(Contacts contact) {

            var client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:54952/api/Contacts/");

            //HTTP POST
            var postTask = client.PostAsJsonAsync("contactDTO", contact);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }

        // GET: Landing/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Landing
        [HttpPost]
        public ActionResult Index(Contacts contact)
        {
            TempData["statusResponse"] = "Error al registrar la información de contacto";
                bool result = createContact(contact);
                if (result) {
                TempData["statusResponse"] = "Tu información ha sido recibida satisfactoriamente";
                }

            TempData["displayModal"] = "ModalResponse";

                return RedirectToAction("Index");
        }
    }
}
