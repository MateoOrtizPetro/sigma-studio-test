﻿using System.Collections.Generic;
using System.Linq;
using SigmaStudioProjectMO.Models;
using System.Web.Http;

namespace SigmaStudioProjectMO.Controllers
{
    [RoutePrefix("api/Contacts")]
    public class ContactsController : ApiController
    {
        // GET: Contacts
        [HttpGet]
        public List<contacts> Get()
        {
            List<contacts> listContacts = new List<contacts>();
            using (ModelDB dbModel = new ModelDB())
            {
                listContacts = dbModel.contacts.ToList();
            }
            return listContacts;
        }

        // GET: Contacts/Details/5
        [HttpGet]
        public contacts Details(int id)
        {
            contacts contactModel = new contacts();
            using (ModelDB dbModel = new ModelDB())
            {
                contactModel = dbModel.contacts.Where(x => x.id == id).FirstOrDefault();
            }
            return contactModel;
        }

        // POST: Contact/Create
        [HttpPost]
        public bool Post(ContactsDTO contactDTO)
        {
            try
            {
                contacts contact = new contacts() { name = contactDTO.name, city = contactDTO.city, email = contactDTO.email, state = contactDTO.state };
                using (ModelDB dbModel = new ModelDB())
                {
                    dbModel.contacts.Add(contact);
                    dbModel.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
