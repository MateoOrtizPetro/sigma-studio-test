﻿using Newtonsoft.Json;
using SigmaStudioProjectMO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;


namespace SigmaStudioProjectMO.Controllers
{
    [RoutePrefix("api/Departments")]
    public class DepartmentsController : ApiController
    {
        // GET: Departments
        [HttpGet]
        public async Task<List<Departments>> Get()
        {
            List<Departments> listDepartments = new List<Departments>();
            try
            {
                var httpClient = new HttpClient();
                var jsonDepartments = await httpClient.GetStringAsync(WebConfigurationManager.AppSettings["UrlServiceDepartments"].ToString());
                var JsonList = JsonConvert.DeserializeObject<Dictionary<string, string[]>>(jsonDepartments);
                foreach (var result in JsonList)
                {
                    listDepartments.Add(new Departments() { state = result.Key, city = result.Value.ToList() });
                }
            }
            catch
            {
                return null;
            }
            return listDepartments;
        }

        [HttpGet]
        public async Task<Departments> Get(string state)
        {
            List<Departments> listDepartments = new List<Departments>();
            try
            {
                var httpClient = new HttpClient();
                var jsonDepartments = await httpClient.GetStringAsync(WebConfigurationManager.AppSettings["UrlServiceDepartments"].ToString());
                var JsonList = JsonConvert.DeserializeObject<Dictionary<string, string[]>>(jsonDepartments);
                foreach (var result in JsonList)
                {
                    listDepartments.Add(new Departments() { state = result.Key, city = result.Value.ToList() });
                }
            }
            catch
            {
                return null;
            }
            return listDepartments.Where(x => x.state.Equals(state)).FirstOrDefault();
        }
    }
}