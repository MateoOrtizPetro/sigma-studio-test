﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SigmaStudioProjectMO.Models
{
    public class ContactsDTO
    {
        public string name { get; set; }
        public string email { get; set; }
        public string state { get; set; }
        public string city { get; set; }
    }
}